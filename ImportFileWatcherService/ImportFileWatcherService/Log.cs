﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportFileWatcherService
{
    public enum ServiceErrorType
    {
        INFO,
        ERROR
    }
    public class Log
    {
        public static Log Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock (_SyncRoot)
                    {
                        if (_Instance == null)
                            _Instance = new Log();
                    }
                }

                return _Instance;
            }
        }

        private static volatile Log _Instance;
        private static object _SyncRoot = new Object();

        private Log()
        {
            LogFileName = "Example";
            LogFileExtension = ".log";
        }

        public StreamWriter Writer { get; set; }

        public string LogPath { get; set; }

        public string LogFileName { get; set; }

        public string LogFileExtension { get; set; }

        public string LogFile { get { return LogFileName + LogFileExtension; } }

        public void WriteLineToLog(String inLogMessage,string folder)
        {
            WriteToLog(inLogMessage + Environment.NewLine,folder);
        }

        public void WriteToLog(String inLogMessage,string folder)
        {
            if (!Directory.Exists(Path.Combine(LogPath,folder)))
            {
                Directory.CreateDirectory(Path.Combine(LogPath, folder));
            }
            //if (Writer == null)
            //{
            //    Writer = new StreamWriter(Path.Combine(LogPath,folder,LogFile), true);
            //}
            //if (!File.Exists(Path.Combine(LogPath, folder, LogFile)))
            //{
            //    File.Create(Path.Combine(LogPath, folder, LogFile));
            //    File.AppendAllText(Path.Combine(LogPath, folder, LogFile), "********* Log File Created for folder " + folder + " on " + DateTime.Now.ToLongTimeString() + "*********" + Environment.NewLine);
            //}
            File.AppendAllText(Path.Combine(LogPath, folder, LogFile), inLogMessage );
            
            //using (StreamWriter w = File.AppendText(Path.Combine(LogPath, folder, LogFile)))
            //{
            //    w.Write(inLogMessage);
            //    w.Flush();
            //}

            //Writer.Write(inLogMessage);
            //Writer.Flush();
        }

        public static void WriteLine(string inLogMessage, string folder, string ServiceLogNumber, ServiceErrorType svcErrType)
        {
            Instance.WriteLineToLog("Service[" + ServiceLogNumber + "] : >>"+svcErrType.ToString() +">>" + inLogMessage,folder);
        }

    }
}
