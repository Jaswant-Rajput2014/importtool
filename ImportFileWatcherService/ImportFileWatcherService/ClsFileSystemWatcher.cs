﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using _onestop;

namespace ImportFileWatcherService
{
  
    public class ClsFileSystemWatcher : FileSystemWatcher
    {
        public ClsFileSystemWatcher()
        {
            Init();
        }
 
        public ClsFileSystemWatcher(String inDirectoryPath)
            : base(inDirectoryPath)
        {
            Init();
        }
 
        public ClsFileSystemWatcher(String inDirectoryPath, string inFilter)
            : base(inDirectoryPath, inFilter)
        {
            Init();
        }
 
        private void Init()
        {
            IncludeSubdirectories = true;
            // Eliminate duplicates when timestamp doesn't change
            NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                   | NotifyFilters.FileName | NotifyFilters.DirectoryName; // The default also has NotifyFilters.LastWrite
            EnableRaisingEvents = true;
            Created += Watcher_Created;
            Filter = misc.getAppConfigValueString("WatchFilterTypes").Replace(";", "|");
            //Changed += Watcher_Changed;
            //Deleted += Watcher_Deleted;
            //Renamed += Watcher_Renamed;
        }
 
        public void Watcher_Created(object source, FileSystemEventArgs inArgs)
        {
            //Log.WriteLine("File created or added: " + inArgs.FullPath);
            string sFile = inArgs.FullPath;

            Console.WriteLine("processing file : " + sFile);

            // Wait if file is still open
            FileInfo fileInfo = new FileInfo(sFile);
            while (IsFileLocked(fileInfo))
            {
                Thread.Sleep(500);
            }
            if (File.Exists(sFile))
            {  
                ClsInitialiseImport.InitialiseFileImport(inArgs.FullPath,fileInfo.Name,fileInfo.Extension);

            }
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open,
                         FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
 
        
    }
}

