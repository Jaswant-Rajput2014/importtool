﻿using _onestop;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.FtpClient;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImportFileWatcherService
{
    public partial class Service1 : ServiceBase
    {
        protected FileSystemWatcher Watcher;
        string PathToFolder = misc.getAppConfigValueString("WatchDirectoryPath");
        private Timer Schedular;

        public string ServiceLogNumber { get; set; }
        public Service1()
        {
            Initialise();
        }
        

        protected override void OnStart(string[] args)
        {
            //Thread thrMain = new Thread(StartWatching);
            //thrMain.Start();


            if (misc.getAppConfigValueString("UseFileSystemWatcher") == "1" && misc.getAppConfigValueString("UseAutoFileWatcher") == "1")
            {                
                Watcher = new ClsFileSystemWatcher(PathToFolder);
               
            }
            
            this.ScheduleService();
        }

        [Conditional("DEBUG_SERVICE")]
        private static void DebugMode()
        {
            Debugger.Break();
        }

        public void Initialise()
        {
            Log.Instance.LogPath = misc.getAppConfigValueString("WatchLogPath");
            string sYear = DateTime.Now.Year.ToString();
            string sMonth = DateTime.Now.Month.ToString();
            string sDay = DateTime.Now.Day.ToString();
            string sErrorTime = sYear + sMonth + sDay;
            Log.Instance.LogFileName = "ServiceLog" + sErrorTime;
        }

        


        public void ScheduleService()
        {
            try
            {
                this.ServiceLogNumber = new Random().Next().ToString();
                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();
                string sErrorTime = sYear + sMonth + sDay;
                Log.Instance.LogFileName = "ServiceLog" + sErrorTime;

                Schedular = new Timer(new TimerCallback(SchedularCallback));
                string mode = misc.getAppConfigValueString("Mode").ToUpper();
                Log.WriteLine("Service Mode: " + mode,"service",this.ServiceLogNumber, ServiceErrorType.INFO);
               

                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;

                if (mode == "DAILY")
                {
                    //Get the Scheduled Time from AppSettings.
                    scheduledTime = DateTime.Parse(misc.getAppConfigValueString("ScheduledTime"));
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next day.
                        scheduledTime = scheduledTime.AddDays(1);
                    }
                }

                if (mode.ToUpper() == "INTERVAL")
                {
                    //Get the Interval in Minutes from AppSettings.
                    int intervalMinutes = Convert.ToInt32(misc.getAppConfigValueString("IntervalMinutes"));

                    //Set the Scheduled Time by adding the Interval to Current Time.
                    scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next Interval.
                        scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                    }
                }

                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);

                Log.WriteLine("Service scheduled to run after: " + schedule ,"service",this.ServiceLogNumber ,ServiceErrorType.INFO);

                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);

                //Change the Timer's Due Time.
                Schedular.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                Log.WriteLine("Service Error on: {0} " + ex.Message + ex.StackTrace, "service", this.ServiceLogNumber, ServiceErrorType.ERROR);

                //Stop the Windows Service.
                using (System.ServiceProcess.ServiceController serviceController = new System.ServiceProcess.ServiceController("ImportFileWatcherService"))
                {
                    serviceController.Stop();
                }
            }
        }

        private void SchedularCallback(object e)
        {
           
            if (misc.getAppConfigValueString("UseFTPWatcher") == "1")
            {
                Log.WriteLine("Service Call back : Calling FTP Import ","service",this.ServiceLogNumber,ServiceErrorType.INFO);
                ListParentChildren();
            }
            else
            {
                Log.WriteLine("Service Call back : Calling File System import ","service",this.ServiceLogNumber,ServiceErrorType.INFO);
                automateFileImport();
            }

            this.ScheduleService();
        }

        public static string[] GetFiles(string path, string searchPattern, SearchOption searchOption)
        {
            string[] searchPatterns = searchPattern.Split('|');
            List<string> files = new List<string>();
            foreach (string sp in searchPatterns)
                files.AddRange(System.IO.Directory.GetFiles(path, sp, searchOption));
            files.Sort();
            return files.ToArray();
        }

        private void automateFileImport()
        {
            string[] filePaths = GetFiles(misc.getAppConfigValueString("WatchDirectoryPath"), misc.getAppConfigValueString("WatchFilterTypes"), SearchOption.AllDirectories); 
                                         
            //Load files of last day and today only.
            
            foreach (string name in filePaths)
            {
                // Check for today and yesterday file.
                try
                {

                
                    //Wait if file is completely written to watch directory.
                    FileInfo fileInfo = new FileInfo(name);
                    string strTimeStamp = fileInfo.Name.Substring(9, 8);
                    while (ClsFileSystemWatcher.IsFileLocked(fileInfo))
                    {
                        Thread.Sleep(500);
                    }
                    if (File.Exists(name))
                    {
                        Log.WriteLine("File created or added: " + name, "service" , this.ServiceLogNumber, ServiceErrorType.INFO);

                        Result result = ClsInitialiseImport.InitialiseFileImport(fileInfo.FullName, fileInfo.Name,fileInfo.Extension);
                    }
                }
                catch (Exception ex1)
                {

                    Log.WriteLine("Error  while Processing File in automateFileImport :" + name + " / Exception : " + ex1.Message.ToString(), "service",this.ServiceLogNumber,ServiceErrorType.ERROR);
                }
                finally
                {
                    Thread.Sleep(Convert.ToInt32(misc.getAppConfigValueString("DelayProcessMinutes"))); // as per Prashant's Request make delay of 2 minutes to avoid dead lock .. 
                }

            }
        }

        public void ListParentChildren()
        {
            FtpClient ftpClient = new FtpClient();
            
            ftpClient.Host = misc.getAppConfigValueString("ftpHost");
            ftpClient.Credentials = new NetworkCredential(misc.getAppConfigValueString("ftpuser"), misc.getAppConfigValueString("ftppassword"));
            
            var path = misc.getAppConfigValueString("WatchDirectoryPath");
            ftpClient.ConnectTimeout = 3000000;
            ftpClient.DataConnectionReadTimeout = 3000000;
            ftpClient.DataConnectionConnectTimeout = 3000000;
            ftpClient.ReadTimeout = 3000000;
            ftpClient.SocketKeepAlive = true;
            //ftpClient.SocketPollInterval = 0;
            ftpClient.EnableThreadSafeDataConnections = false;
            ftpClient.Connect();
            var listing = ftpClient.GetListing(path, FtpListOption.Modify | FtpListOption.Size );

            foreach (FtpListItem item in listing)
            {
                if(item.Type == FtpFileSystemObjectType.Directory)
                {
                    DownloadAndProcessFiles(item.FullName);
                }
            }
            
            
        }
               
        private void DownloadAndProcessFiles(string folder)
        {
            using (var ftpClient = new FtpClient())
            {
                ftpClient.Host = misc.getAppConfigValueString("ftpHost");
                ftpClient.Credentials = new NetworkCredential(misc.getAppConfigValueString("ftpuser"), misc.getAppConfigValueString("ftppassword"));
                //var path = misc.getAppConfigValueString("WatchDirectoryPath");
                var path = folder;
                
                ftpClient.ConnectTimeout = 3000000;
                ftpClient.DataConnectionReadTimeout = 3000000;
                ftpClient.DataConnectionConnectTimeout = 3000000;
                ftpClient.ReadTimeout = 3000000;
                ftpClient.SocketKeepAlive = true;
                //ftpClient.SocketPollInterval = 0;
                ftpClient.EnableThreadSafeDataConnections = false;
                ftpClient.Connect();
                var a = ftpClient.GetListing(path, FtpListOption.Modify | FtpListOption.Size );
               
                
                // List all files with a .xlsx extension
                foreach (var ftpListItem in
                    ftpClient.GetListing(path, FtpListOption.Modify | FtpListOption.Size)
                        //.Where(ftpListItem => string.Equals(Path.GetExtension(ftpListItem.Name), misc.getAppConfigValueString("WatchFilterTypes").Replace("*", ""))))
                        .Where(ftpListItem => misc.getAppConfigValueString("WatchFilterTypes").Replace("*", "").Split(new char[] { '|' }).ToArray().Contains(Path.GetExtension(ftpListItem.Name))))
                {
                    try 
                    {
                        //download file to local path
                        var destinationPath = string.Format(@"{0}{1}\{2}", misc.getAppConfigValueString("FTPDownloadFolder") ,Directory.GetParent(ftpListItem.FullName).Name, ftpListItem.Name);
                        //Create Directory if not present
                        if (!Directory.Exists(Directory.GetParent(destinationPath).FullName))
                        {
                            Directory.CreateDirectory(Directory.GetParent(destinationPath).FullName);
                        }
                        using (var ftpStream = ftpClient.OpenRead(ftpListItem.FullName))
                        {
                            // Download file at filesystem, later on this file will be sent to importexcel
                            using (var fileStream = File.Create(destinationPath, (int)ftpStream.Length))
                            {
                            
                                var buffer = new byte[8 * 1024];
                                int count;
                                while ((count = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    fileStream.Write(buffer, 0, count);
                                }

                            }
                            // Note: In FTP, we donot delete the file and keep a backup copy of FTP file to local
                        
                            Result result = new Result();
                       
                            if (File.Exists(destinationPath))
                            {

                                Log.WriteLine("File Moved for processing to destination folder : " + destinationPath,"service" ,this.ServiceLogNumber,ServiceErrorType.INFO);
                                //Read File completely before read
                                FileInfo fileInfo = new FileInfo(destinationPath);
                                string strTimeStamp = fileInfo.Name.Substring(9, 8);
                                while (ClsFileSystemWatcher.IsFileLocked(fileInfo))
                                {
                                    Thread.Sleep(500);
                                }
                                //First one is source file path and second is file name sent for processing
                                result = ClsInitialiseImport.InitialiseFileImport(ftpListItem.FullName,fileInfo.Name,fileInfo.Extension);
                            }
                            //move file to another location on FTP


                            if (result.Success)
                            {
                                if (!ftpClient.DirectoryExists( misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(ftpListItem.FullName).Name + "/Processed/"))
                                {
                                    ftpClient.CreateDirectory(misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(ftpListItem.FullName).Name + "/Processed/", true);
                                }
                                ftpClient.Rename(ftpListItem.FullName, misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(ftpListItem.FullName).Name + "/Processed/" + ftpListItem.Name.ToString());
                            }
                            else
                            {
                                if (!ftpClient.DirectoryExists(misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(ftpListItem.FullName).Name + "/Errored/"))
                                {
                                    ftpClient.CreateDirectory(misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(ftpListItem.FullName).Name + "/Errored/", true);
                                }
                                ftpClient.Rename(ftpListItem.FullName, misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(ftpListItem.FullName).Name + "/Errored/" + ftpListItem.Name.ToString());
                            }

                        }

                    }
                    catch (Exception ex1)
                    {

                        Log.WriteLine("Error  while Processing File in FTP-DownloadAndProcessImport  :" + ftpListItem.FullName + " / Exception : " + ex1.Message.ToString(), "service", this.ServiceLogNumber, ServiceErrorType.ERROR);
                    }
                    finally
                    {
                        Thread.Sleep(Convert.ToInt32(misc.getAppConfigValueString("DelayProcessMinutes"))); // as per Prashant's Request make delay of 2 minutes to avoid dead lock .. 
                    }
                }

                   
                }
            }
              
 
        protected override void OnStop()
        {
            Log.WriteLine("Schedular Service Stopped at : " + DateTime.Now.ToString(),"service",this.ServiceLogNumber,ServiceErrorType.INFO);
            this.Schedular.Dispose();
        }

        
    }
}
