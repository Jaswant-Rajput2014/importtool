﻿using _onestop;
using onestop.common;
using Onestop.Core.Datastore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ImportFileWatcherService
{
    public class Result
    {
        public List<string> Errors = new List<string>();
        public bool Success = false;
        public bool NeedConfirmation = false;
        public int ProcessImportID = -1;
        public bool Scheduled = false;
    }

    public class ClsProcessImport
    {
        private const string ImportDatabase = "StoreImports";

       

        #region Import Functions
        /// <summary>
        /// Imports an Excel spreadsheet into a DB staging table for further processing
        /// </summary>
        /// <param name="fileName">The path to the file on the server</param>
        /// <param name="validateOnly">Whether to actually import this file (false) or simply run code validation on it (true)</param>
        public static Result ImportExcel(string fileName, int importType, bool validateOnly, string shortFileName, DateTime? scheduledDate, string fileExtension, int clientid)
        {
            Result result = new Result();
            // Generate the connection string for Excel file.
            string excelConn = "";

            // There is no column name In a Excel spreadsheet. 
            // So we specify "HDR=YES" in the connection string to use 
            // the values in the first row as column names. 
            excelConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties='Excel 12.0 xml;HDR=YES;IMEX=1'";

            DataTable dtExcel = null;
            result.Success = true;
            try
            {

                if (fileExtension == ".xlsx")
                    dtExcel = GetExcelData(excelConn);
                else if (fileExtension == ".csv")
                    dtExcel = CsvFileToDatatable(fileName, true);
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Errors.Add(ex.Message);
                email.ErrorLogger("Import Tool: RetrieveData - ", ex.Message);
                return result;
            }

            if (dtExcel != null)
            {
                //validate the data
                result = ValidateExcelData(dtExcel, importType, validateOnly, shortFileName, scheduledDate, fileExtension);

                if (result.Success)
                {
                    if (!validateOnly)
                    {
                        // Import the data.
                        try
                        {
                            Result bulkCopyResult = SqlBulkCopyImport(dtExcel, importType, shortFileName, scheduledDate, result.NeedConfirmation, clientid);
                            result.ProcessImportID = bulkCopyResult.ProcessImportID;
                            if (!bulkCopyResult.Success)
                            {
                                result.Success = false;
                                result.Errors.AddRange(bulkCopyResult.Errors);
                            }
                        }
                        catch (Exception ex)
                        {
                            result.Success = false;
                            result.Errors.Add(ex.Message);
                            email.ErrorLogger("Import Tool: BulkCopyImport - ", ex.Message);
                        }
                    }
                }
            }
            else
            {
                result.Errors.Add("Problem retrieving excel data");
                return result;
            }

            return result;
        }

        /// <summary>
        /// Retrieves data from an Excel spreadsheet.
        /// </summary>
        /// <param name="strConn">the connection string including the file path and extra parameters</param>
        /// <returns></returns>
        protected static DataTable GetExcelData(string strConn)
        {
            DataTable dtExcel = new DataTable();

            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                conn.Open();
                //get the sheetname of the first sheet
                DataTable dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = "";
                string rsheetName = "";

                if (dt == null || dt.Rows.Count == 0)
                    return null;

                foreach (DataRow row in dt.Rows)
                {
                    rsheetName = row["TABLE_NAME"].ToString();
                    if (!rsheetName.EndsWith("$"))
                    {
                        continue;
                    }
                    else
                    {
                        sheetName = row["TABLE_NAME"].ToString();
                    }

                }    
                
                //string sheetName = dt.Rows[0]["TABLE_NAME"].ToString();

                // Initialize an OleDbDataAdapter object.
                OleDbDataAdapter da = new OleDbDataAdapter("select * from [" + sheetName + "]", conn);

                // Fill the DataTable with data from the Excel spreadsheet.
                da.Fill(dtExcel);

                conn.Close();
                conn.Dispose();
            }

            //check for and remove any blank rows
            List<DataRow> rowsToDelete = new List<DataRow>();
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                int blankColumns = 0;
                for (int j = 0; j < dtExcel.Columns.Count; j++)
                {
                    if (dtExcel.Rows[i][j] == null || string.IsNullOrWhiteSpace(dtExcel.Rows[i][j].ToString()))
                    {
                        blankColumns++;
                    }
                }
                if (blankColumns == dtExcel.Columns.Count)
                {
                    //this row is blank; mark it for deletion
                    dtExcel.Rows[i].Delete();
                }
            }

            //now actually delete any rows that are marked
            dtExcel.AcceptChanges();

            return dtExcel;
        }

        /// <summary>
        /// Validates the excel data based on ImportMetaData table
        /// </summary>
        /// <param name="dtExcel">A data table containing data from the Excel spreadsheet being validated</param>
        /// <returns></returns>
        private static Result ValidateExcelData(DataTable dtExcel, int importType, bool validateOnly, string fileName, DateTime? scheduledDate, string fileExtension)
        {
            Result result = new Result();
            result.Success = true;
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("ProcessImportTypeID", importType));
            DataSet importMetaData = utilities.RunSqlCommandDataSet(ImportDatabase + ".dbo.p_GetImportMetaData", CommandType.StoredProcedure, parameters);

            //first check the columns are all valid
            for (int i = 0; i < dtExcel.Columns.Count; i++)
            {
                DataColumn excelColumn = dtExcel.Columns[i];
                try
                {
                    //this makes sure the excel sheet has been formatted to text, so that all data can be read properly
                    //apparently, boolean does not get converted to string even when excel is formatted as text. so allow that too.

                    if (fileExtension != ".csv") // no need to Format Csv in TEXT.
                    {
                        if (excelColumn.DataType != typeof(String) && excelColumn.DataType != typeof(Boolean))
                        {
                            result.Errors.Add("The Excel data was not formatted as text. Please make sure to perform the steps mentioned in the 'Add New Import' dialog to format your data.");
                            //immediately fail if not formatted... no point in validating data, it may give a false pass
                            result.Success = false;
                            return result;
                        }
                    }

                    DataRow[] metaColumn = importMetaData.Tables[0].Select("[ColumnName]='" + excelColumn.ColumnName.ToUpper() + "'");
                    if (metaColumn == null || metaColumn[0] == null)
                    {
                        result.Errors.Add("Invalid column: " + excelColumn.ColumnName);
                        result.Success = false;
                    }
                }
                catch (Exception e)
                {
                    result.Errors.Add("Invalid column: " + excelColumn.ColumnName);
                    result.Success = false;
                }
            }

            // Code Commented as per Zohaib's request to not check columns 

            //now check if there are any required columns that are NOT in the excel sheet
            //for (int i = 0; i < importMetaData.Tables[0].Rows.Count; i++)
            //{
            //    DataRow metaDataRow = importMetaData.Tables[0].Rows[i];

            //    if (Convert.ToInt32(metaDataRow["RequirementID"]) == 0)
            //    {
            //        string columnName = metaDataRow["ColumnName"].ToString();

            //        bool found = false;
            //        for (int j = 0; j < dtExcel.Columns.Count; j++)
            //        {
            //            if (dtExcel.Columns[j].ColumnName.ToUpper() == columnName)
            //            {
            //                found = true;
            //            }
            //        }

            //        if (!found)
            //        {
            //            result.Errors.Add("Missing required column: " + columnName);
            //            result.Success = false;
            //        }
            //    }
            //}

            Regex[] regex = new Regex[dtExcel.Columns.Count];
            Regex[] regexWarning = new Regex[dtExcel.Columns.Count];

            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {

                DataRow dr = dtExcel.Rows[i];
                for (int j = 0; j < dtExcel.Columns.Count; j++)
                {
                    DataColumn excelColumn = dtExcel.Columns[j];
                    DataRow[] metaColumn = null;

                    try
                    {
                        metaColumn = importMetaData.Tables[0].Select("[ColumnName]='" + excelColumn.ColumnName.ToUpper() + "'");

                        if (metaColumn == null || metaColumn[0] == null)
                        {
                            continue;
                        }
                    }
                    catch (Exception e)
                    {
                        continue;
                    }
                    DataRow metaColumnInfo = metaColumn[0];

                    //we add 2 to the row number because 0 based index and the first row is the header row with column names
                    int rowNumber = i + 2;

                    //first check required
                    if (Convert.ToInt32(metaColumnInfo["RequirementID"]) == 0)
                    {
                        if (string.IsNullOrWhiteSpace(dr[excelColumn].ToString()))
                        {
                            result.Success = false;
                            result.Errors.Add("Row " + rowNumber + " : Missing required value for column " + excelColumn.ColumnName + ".");
                            continue;
                        }
                    }

                    //now validate the data type is correct, IF this value is filled in:
                    if (!string.IsNullOrWhiteSpace(dr[excelColumn].ToString()))
                    {
                        switch (metaColumnInfo["ColumnDataType"].ToString().ToUpper())
                        {
                            case "INT":
                                int intResult = 0;
                                if (!Int32.TryParse(dr[excelColumn].ToString(), out intResult))
                                {
                                    result.Success = false;
                                    result.Errors.Add("Row " + rowNumber + " : Invalid value for column " + excelColumn.ColumnName + ". Must be an integer.");
                                }
                                break;
                            case "MONEY":
                                decimal decimalResult = 0;
                                if (!Decimal.TryParse(dr[excelColumn].ToString(), out decimalResult))
                                {
                                    result.Success = false;
                                    result.Errors.Add("Row " + rowNumber + " : Invalid value for column " + excelColumn.ColumnName + ". Must be a decimal.");
                                }
                                break;
                            case "BIT":
                                string value = dr[excelColumn].ToString().ToLower();
                                if (value != "true" && value != "false" && value != "0" && value != "1")
                                {
                                    result.Success = false;
                                    result.Errors.Add("Row " + rowNumber + " : Invalid value for column " + excelColumn.ColumnName + ". Must be a boolean (1 or 0, true or false).");
                                }
                                else
                                {
                                    //convert to boolean so this is converted to BIT properly in DB bulk copy later
                                    if (value == "true" || value == "1")
                                    {
                                        dr[excelColumn] = true;
                                    }
                                    else
                                    {
                                        dr[excelColumn] = false;
                                    }
                                }
                                break;
                            case "DATETIME":
                                DateTime dateResult = DateTime.MinValue;
                                if (!DateTime.TryParse(dr[excelColumn].ToString(), out dateResult))
                                {
                                    result.Success = false;
                                    result.Errors.Add("Row " + rowNumber + " : Invalid value for column " + excelColumn.ColumnName + ". Must be a date.");
                                }
                                break;
                            case "NVARCHAR":
                                //check length
                                if (dr[excelColumn].ToString().Length > Convert.ToInt32(metaColumnInfo["ColumnLength"]))
                                {
                                    result.Success = false;
                                    result.Errors.Add("Row " + rowNumber + " : Invalid value for column " + excelColumn.ColumnName + ". Value is too long.");
                                }
                                //else if (excelColumn.ColumnName.Trim().ToLower().Equals("htmlcard") && !utilities.isMetaTag(dr[excelColumn].ToString()))
                                //{
                                //    result.Success = false;
                                //    result.Errors.Add("Row " + rowNumber + " : Invalid value for column " + excelColumn.ColumnName + ". Value must be in meta tag format.");
                                //}
                                break;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(metaColumnInfo["RegexRestriction"].ToString()))
                    {
                        //run a regex match on the column. if it comes back positive, flag this as invalid. I store the regex in a persistent object to increase performance on matching.
                        if (regex[j] == null)
                        {
                            regex[j] = new Regex(metaColumnInfo["RegexRestriction"].ToString());
                        }
                        MatchCollection matches = regex[j].Matches(dr[excelColumn].ToString());
                        if (matches.Count > 0)
                        {
                            result.Success = false;
                            string characterList = "";
                            foreach (Match match in matches)
                            {
                                characterList += match.Value + " ";
                            }
                            result.Errors.Add("Row " + rowNumber + " : Invalid value for column " + excelColumn.ColumnName + ". Value contains invalid characters: " + characterList);
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(metaColumnInfo["RegexWarning"].ToString()))
                    {
                        //run a regex match on the column. if it comes back positive, flag this as invalid. I store the regex in a persistent object to increase performance on matching.
                        if (regexWarning[j] == null)
                        {
                            regexWarning[j] = new Regex(metaColumnInfo["RegexWarning"].ToString());
                        }
                        MatchCollection matches = regexWarning[j].Matches(dr[excelColumn].ToString());
                        if (matches.Count > 0)
                        {
                            result.NeedConfirmation = true;
                            string characterList = "";
                            foreach (Match match in matches)
                            {
                                characterList += match.Value + " ";
                            }
                            result.Errors.Add("Row " + rowNumber + " : Warning for column " + excelColumn.ColumnName + ". Value contains possible invalid value: " + characterList);
                        }
                    }
                }
            }

            return result;
        }

        // Import the data from DataTable to SQL Server via SqlBulkCopy
        protected static Result SqlBulkCopyImport(DataTable dtExcel, int importType, string fileName, DateTime? scheduledDate, bool needsConfirmation, int clientId)
        {
            Result result = new Result();
            result.Success = true;
            string dbTable = "";

            string importTable = GetStageTable(importType);

            if (importTable == null)
            {
                result.Errors.Add("Invalid table. Please make sure the Import Type is correct.");
                result.Success = false;
                return result;
            }

            //this is causing problems, taking it out for now
            //if (CheckForRunningImport() && !scheduledDate.HasValue)
            //{
            //    result.Errors.Add("There is an import currently running. Please wait until the import completes or schedule this import for a future time.");
            //    result.Success = false;
            //    return result;
            //}

            dbTable = ImportDatabase + "." + importTable;

            Guid guid = Guid.NewGuid();

            dtExcel.Columns.Add("TranID", typeof(string));
            dtExcel.Columns.Add("Processed", typeof(int));
            dtExcel.Columns.Add("Status", typeof(int));

            //Convert.ToInt32(ddlClientID.SelectedValue);
            if (clientId == 0)
            {
                result.Errors.Add("Invalid client ID. Please make sure the client is configured correctly.");
                result.Success = false;
                return result;
            }

            //add guid to the rows being imported. this is one guid used per file being imported.
            foreach (DataRow dr in dtExcel.Rows)
            {
                dr["TranID"] = guid;
                //TODO: make sure these are set to the correct values
                dr["Processed"] = 0;
                dr["Status"] = 0;
            }



            using (SqlConnection conn = new SqlConnection(new dbmisc.OS_SqlConnectionString(dbmisc.GetDecryptedConnectionString("ConnectionString")).cs))
            {
                // Open the connection.
                conn.Open();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                {
                    // Specify the destination table name.
                    bulkCopy.DestinationTableName = dbTable;

                    //map the columns correctly, since case or extra spaces may be different in the excel vs the DB
                    DataSet importTableSchema = GetTableSchema(importTable);
                    for (int i = 0; i < importTableSchema.Tables[0].Columns.Count; i++)
                    {

                        DataColumn destColumn = importTableSchema.Tables[0].Columns[i];
                        for (int j = 0; j < dtExcel.Columns.Count; j++)
                        {
                            DataColumn sourceColumn = dtExcel.Columns[j];

                            if (sourceColumn.ColumnName.ToUpper().Trim().Replace(" ", "") == destColumn.ColumnName.ToUpper().Trim().Replace(" ", ""))
                            {
                                bulkCopy.ColumnMappings.Add(sourceColumn.ColumnName, destColumn.ColumnName);
                                break;
                            }
                        }
                    }

                    // Write from the source to the destination.
                    bulkCopy.WriteToServer(dtExcel);
                }
            }

            //_onestop.Users u = new _onestop.Users();
            //System.Web.HttpContext context = System.Web.HttpContext.Current;
            //_onestop.UserDetails uex = u.GetUserDetails(int.Parse(context.User.Identity.Name));

            //import successful, call the sproc to schedule / run the DB data transfer
            string sprocName = ImportDatabase + ".[dbo].[AddProcessImport]"; //importTable[0]["ProcName"].ToString();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("ProcessImportTypeID", importType));
            parameters.Add(new SqlParameter("ClientID", clientId));
            parameters.Add(new SqlParameter("ImportStatusID", 3));
            parameters.Add(new SqlParameter("ImportFileName", fileName));
            parameters.Add(new SqlParameter("UserName", "admin@onestop.com"));
            //parameters.Add(new SqlParameter("RunDateTime", null));
            //parameters.Add(new SqlParameter("Comments", null));
            //parameters.Add(new SqlParameter("CreateDate", null));
            //parameters.Add(new SqlParameter("ModifyDate", null));
            //parameters.Add(new SqlParameter("ClientPrefix", null));
            SqlParameter processId = new SqlParameter();
            processId.ParameterName = "ProcessImportID";
            processId.SqlDbType = SqlDbType.Int;
            processId.Direction = ParameterDirection.Output;
            processId.Size = 4;
            parameters.Add(processId);


            //parameters.Add(new SqlParameter("userid", User.Identity.Name));
            parameters.Add(new SqlParameter("TranID", guid.ToString()));


            bool runNow = false;
            if (scheduledDate.HasValue)
            {
                parameters.Add(new SqlParameter("ScheduledRunDateTime", scheduledDate.Value));
            }
            else
            {
                parameters.Add(new SqlParameter("ScheduledRunDateTime", DateTime.Now));
                runNow = true;
            }

            utilities.RunSqlCommand(sprocName, CommandType.StoredProcedure, parameters);
            int processImportId;
            if (parameters[5].Value != null && int.TryParse(parameters[5].Value.ToString(), out processImportId))
            {
                if (needsConfirmation)
                {
                    //don't run the import yet, store the import id and whether to run now, so the user can accept or cancel the import.
                    result.Scheduled = !runNow;
                    result.ProcessImportID = processImportId;
                }
                else
                {
                    ProcessImport(processImportId, runNow);
                }
            }
            else
            {
                result.Errors.Add("There was an issue attempting to schedule the import.");
                result.Success = false;
            }

            return result;
        }

        private static void ProcessImport(int processImportId, bool runNow)
        {
            List<SqlParameter> progressParameters = new List<SqlParameter>();
            progressParameters.Add(new SqlParameter("ProcessImportID", processImportId));
            utilities.RunSqlCommand(ImportDatabase + ".dbo.p_UpdateProcessImportProgress", CommandType.StoredProcedure, progressParameters);

            List<SqlParameter> progressParameters2 = new List<SqlParameter>();
            progressParameters2.Add(new SqlParameter("ProcessImportID", processImportId));
            utilities.RunSqlCommand(ImportDatabase + ".dbo.p_UpdateProcessImportProgress", CommandType.StoredProcedure, progressParameters2);

            if (runNow)
            {
                List<SqlParameter> runParameters = new List<SqlParameter>();
                runParameters.Add(new SqlParameter("ProcessImportID", processImportId));
                utilities.RunSqlCommand(ImportDatabase + ".dbo.p_RunImport", CommandType.StoredProcedure, runParameters, true, 3600);
            }
            else
            {
                List<SqlParameter> updateParameters = new List<SqlParameter>();
                updateParameters.Add(new SqlParameter("ProcessImportID", processImportId));
                SqlParameter importStatus = new SqlParameter("ImportStatusID", 2);
                importStatus.SqlDbType = SqlDbType.SmallInt;
                importStatus.Value = 2;
                updateParameters.Add(importStatus);
                utilities.RunSqlCommand(ImportDatabase + ".dbo.p_UpdateProcessImportStatus", CommandType.StoredProcedure, updateParameters);
            }
        }

        public static DataTable CsvFileToDatatable(string path, bool IsFirstRowHeader)
        {
            Result result = new Result();
            string header = "No";
            string sql = string.Empty;
            DataTable dataTable = null;
            string pathOnly = string.Empty;
            string fileName = string.Empty;
            try
            {
                pathOnly = Path.GetDirectoryName(path);
                fileName = Path.GetFileName(path);
                sql = @"SELECT * FROM [" + fileName + "]";
                if (IsFirstRowHeader)
                {
                    header = "Yes";
                }
                using (OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +";Extended Properties=\"Text;HDR=" + header + "\""))
                {
                    using (OleDbCommand command = new OleDbCommand(sql, connection))
                    {
                        using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
                        {
                            dataTable = new DataTable();
                            adapter.Fill(dataTable);
                        }
                    }
                }
            }
            finally
            {
            }
            return dataTable;
        }



        #endregion

        #region SQL Helpers

        private static DataSet GetTableSchema(string tableName)
        {
            string sql = "SELECT TOP 0 * FROM " + ImportDatabase + "." + tableName;
            return utilities.RunSqlCommandDataSet(sql, CommandType.Text);
        }

        private static DataSet GetOldImports(int clientId)
        {
            string sql = "SELECT ImportFileName FROM " + ImportDatabase + ".dbo.ProcessImport WHERE CreateDate < DATEADD(day, -7, GETDATE()) AND CreateDate > DATEADD(day, -30, GETDATE()) AND CLIENTID = @ClientID";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("ClientID", clientId));
            return utilities.RunSqlCommandDataSet(sql, CommandType.Text, parameters);
        }

        private static string GetStageTable(int processImportTypeID)
        {
            string sql = "SELECT ObjectName FROM " + ImportDatabase + ".dbo.ProcessImportTypeObjectInfo WHERE ProcessImportTypeID = " + processImportTypeID + " AND ObjectDesc='Staging Table'";
            return utilities.RunSqlCommandScalar(sql, CommandType.Text);
        }

        private static bool CheckFileNameExists(string fileName)
        {
            DataSet tableSchema = new DataSet();
            string sql = "SELECT TOP 1 1 FROM " + ImportDatabase + ".dbo.ProcessImport WHERE ImportFileName = @FileName AND ImportStatusID <> 5 AND ImportStatusID <> -1;";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("FileName", fileName));
            tableSchema = utilities.RunSqlCommandDataSet(sql, CommandType.Text, parameters);
            return tableSchema.Tables[0].Rows.Count > 0;
        }

        private static bool CheckForRunningImport()
        {
            DataSet tableSchema = new DataSet();
            string sql = "SELECT TOP 1 1 FROM " + ImportDatabase + ".dbo.ProcessImport WHERE ImportStatusID = 3;";
            tableSchema = utilities.RunSqlCommandDataSet(sql, CommandType.Text);
            return tableSchema.Tables[0].Rows.Count > 0;
        }

        #endregion

      
    }
}
