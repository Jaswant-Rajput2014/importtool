﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Globalization;

using System.Text;
using System.Xml;
using System.IO;
using Onestop.Core.Datastore;
using onestop.common;
using _onestop;


namespace ImportFileWatcherService
{
    public class ListIPRanges
    {
        public Decimal FROM_IP { get; set; }
        public Decimal TO_IP { get; set; }
    }

    /// <summary>
    /// This class contains all the common utilities/commmon functions 
    /// by NitinA on 08/16/2009
    /// </summary>
    public static class utilities
    {

         
        public static void RunSqlCommand(string commandText, CommandType commandType, List<SqlParameter> parameters = null, bool async = false, int timeout = 0)
        {
            var connectionstring = new dbmisc.OS_SqlConnectionString(misc.getAppConfigValueString("ConnectionString")).cs;
            var connection = new SqlConnection(connectionstring);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = commandType;
            if (parameters != null)
            {
                for (int i = 0; i < parameters.Count; i++)
                {
                    command.Parameters.Add(parameters[i]);
                }
            }
            if (timeout > 0)
            {
                command.CommandTimeout = timeout;
            }
            connection.Open();
            connection.BeginTransaction(IsolationLevel.ReadUncommitted).Commit();
            //call using async if specified, so it doesn't lock the application
            if (async)
            {
                AsyncCallback callback = new AsyncCallback(RunSqlCommandResponse);
                command.BeginExecuteNonQuery(callback, command);
            }
            else
            {
                command.ExecuteNonQuery();
                connection.Close();
                connection.Dispose();
                command.Dispose();
            }
        }

        public static void RunSqlCommandResponse(IAsyncResult result)
        {
            SqlCommand command = null;
            try
            {
                command = (SqlCommand)result.AsyncState;
                int rowCount = command.EndExecuteNonQuery(result);
                SqlConnection connection = command.Connection;
                connection.Close();
                connection.Dispose();
                command.Dispose();
            }
            catch (Exception ex)
            {
                string errorSource = "RunSqlCommandResponse: Problem running SQL Async";
                if (command != null)
                {
                    errorSource += " : " + command.CommandText + " - ";
                }
                email.ErrorLogger(errorSource, ex.Message);
            }
        }

        public static DataSet RunSqlCommandDataSet(string commandText, CommandType commandType, List<SqlParameter> parameters = null, int timeout = 120)
        {
            DataSet dataSet = new DataSet();
            var connectionstring = new dbmisc.OS_SqlConnectionString(misc.getAppConfigValueString("ConnectionString")).cs;
            var connection = new SqlConnection(connectionstring);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = commandType;
            command.CommandTimeout = timeout;
            if (parameters != null)
            {
                for (int i = 0; i < parameters.Count; i++)
                {
                    command.Parameters.Add(parameters[i]);
                }
            }

            connection.Open();
            var adapter = new SqlDataAdapter(command);
            adapter.Fill(dataSet);
            connection.Close();
            connection.Dispose();
            command.Dispose();

            return dataSet;
        }

        public static string RunSqlCommandScalar(string commandText, CommandType commandType, List<SqlParameter> parameters = null, int timeout = 120)
        {
            DataSet dataSet = new DataSet();
            var connectionstring = new dbmisc.OS_SqlConnectionString(misc.getAppConfigValueString("ConnectionString")).cs;
            var connection = new SqlConnection(connectionstring);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = commandType;
            command.CommandTimeout = timeout;
            if (parameters != null)
            {
                for (int i = 0; i < parameters.Count; i++)
                {
                    command.Parameters.Add(parameters[i]);
                }
            }

            connection.Open();
            object scalar = command.ExecuteScalar();
            connection.Close();
            connection.Dispose();
            command.Dispose();

            return scalar.ToString();
        }

        public static string RunSqlCommandScalarWithConnectionString(string conn, string commandText, CommandType commandType, List<SqlParameter> parameters = null, int timeout = 120)
        {
            DataSet dataSet = new DataSet();
            var connectionstring = new dbmisc.OS_SqlConnectionString(conn).cs;
            var connection = new SqlConnection(connectionstring);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = commandType;
            command.CommandTimeout = timeout;
            if (parameters != null)
            {
                for (int i = 0; i < parameters.Count; i++)
                {
                    command.Parameters.Add(parameters[i]);
                }
            }

            connection.Open();
            object scalar = command.ExecuteScalar();
            connection.Close();
            connection.Dispose();
            command.Dispose();

            return scalar.ToString();
        }
    }
}
