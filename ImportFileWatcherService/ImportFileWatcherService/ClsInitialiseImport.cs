﻿using _onestop;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportFileWatcherService
{
    public class ClsInitialiseImport
    {
        private const string ImportDatabase = "StoreImports";
        private const string OsCommonDatabase="os_common";
        enum ProcessImportTypes
        {
            ProductImport = 7,
            PriceAdjustmentImport =9,
            MetatagImport =11,
            MarketingTagImport=18,
            CategoryAssignmentsImport=8,
            CategoryAttributeImport=17,
            ColorParentAssignmentImport=20,
            ProductSpecImport=15,
            SubscriberImport=16
        };

        public string ServiceLogNumber { get; set; }
        public static Result InitialiseFileImport(string watchFileFullName, string watchFileName, string watchFileExtension)
        { 
            int clientId=0;
            int fileType=0;
            string strTimeStamp=string.Empty;
            string sourceDirDownloadFileFullPath = string.Empty;
            string clientprefix = string.Empty;
            //Validate File Format
            Result result = new Result();

            try
            {
                
                if (validateFileNameFormat(watchFileFullName, watchFileName, out clientId, out fileType, out  strTimeStamp,out clientprefix))
                {
                    //1. Copy file to SourceDir
                
                    if (misc.getAppConfigValueString("UseFTPWatcher") == "0")
                    {
                        sourceDirDownloadFileFullPath = misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(watchFileFullName).Name + "\\" + watchFileName;
                        TrfFile(watchFileFullName, sourceDirDownloadFileFullPath, "copy");

                        //4. delete file from watch location
                        if (misc.getAppConfigValueString("UseFTPWatcher") == "0")
                        {
                            TrfFile(watchFileFullName, null, "delete");
                        }
                    }
                    else
                    {
                        //For FTP, file downloaded to local and then sent for processing
                        sourceDirDownloadFileFullPath = misc.getAppConfigValueString("FTPDownloadFolder").ToString() + Directory.GetParent(watchFileFullName).Name + "\\" + watchFileName;
                    }

                    
                    //2. Run File Import
                    result = RunImport(new ImportFileAttrb()
                        {
                            FileName = sourceDirDownloadFileFullPath,
                            ImportType = Convert.ToInt32(fileType),
                            ValidateOnly = false,
                            ShortFileName = watchFileName,
                            ScheduledDate = null,
                            FileExtension = watchFileExtension,
                            ClientID = clientId

                        });
                    
                }
                else
                {
                    result = new Result() { Success = false };
                    if (misc.getAppConfigValueString("UseFTPWatcher") == "1")
                    {
                        return result;
                    }
                    else
                    {
                        //For FileSystem
                    
                        sourceDirDownloadFileFullPath = misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(watchFileFullName).Name + "\\" + watchFileName;
                        TrfFile(watchFileFullName, sourceDirDownloadFileFullPath, "copy");

                        //4. delete file from watch location
                        if (misc.getAppConfigValueString("UseFTPWatcher") == "0")
                        {
                            TrfFile(watchFileFullName, null, "delete");
                        }
                    }
                }

                //3. Copy file to Processed folder/ Error folder.
                if (misc.getAppConfigValueString("UseFTPWatcher") == "0")
                {
                    if (result != null && result.Success)
                    {
                        //move file to processed folder
                        TrfFile(sourceDirDownloadFileFullPath, misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(sourceDirDownloadFileFullPath).Name + "\\Processed\\" + watchFileName, "move");
                    }
                    else
                    {
                        //move file to error folder
                        TrfFile(sourceDirDownloadFileFullPath, misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(sourceDirDownloadFileFullPath).Name + "\\Errored\\" + watchFileName, "move");
                    }
                }

            }
            catch (Exception ex)
            {

                Log.WriteLine("Excepton While Processing File " + watchFileFullName + "/ Exception :" + ex.Message.ToString(),clientprefix,"",ServiceErrorType.ERROR);
            }

            WriteErrorstoLog(result, clientprefix, misc.getAppConfigValueString("SourceDir").ToString() + Directory.GetParent(sourceDirDownloadFileFullPath).Name + "\\Errored\\" + watchFileName);
           return result;
        }
        
        private static void WriteErrorstoLog(Result result,string folder, string destFileName)
        {
            if(result.Errors.Count >0)
            {
                Log.WriteLine("*********** Errors Collected by Result, errors listed as below in File : " + destFileName + " *************", folder, "", ServiceErrorType.INFO);
                foreach (string item in result.Errors)
                {
                    Log.WriteLine(item, folder, "", ServiceErrorType.ERROR);
                }
                Log.WriteLine("*********** Errors Collected by Result End ****************************************************************", folder, "", ServiceErrorType.INFO);
            }
        }
        public static void TrfFile (string srcFileFullName , string destFileFullName, string op)
        {
            
            switch (op)
	        {
                case "copy":
                    if (!Directory.Exists(Directory.GetParent(destFileFullName).FullName))
                    {
                        Directory.CreateDirectory(Directory.GetParent(destFileFullName).FullName);
                    }
                    if(ClsFileSystemWatcher.IsFileLocked(new FileInfo(srcFileFullName)))
                    {
                        System.Threading.Thread.Sleep(5000);
                    }
                    if (!File.Exists(destFileFullName))
                        File.Copy(srcFileFullName, destFileFullName);
                    break;
                case "move":
                    if (!Directory.Exists(Directory.GetParent(destFileFullName).FullName))
                    {
                        Directory.CreateDirectory(Directory.GetParent(destFileFullName).FullName);
                    }
                    if (ClsFileSystemWatcher.IsFileLocked(new FileInfo(srcFileFullName)))
                    {
                        System.Threading.Thread.Sleep(5000);
                    }
                    if (!File.Exists(destFileFullName))
                    {
                        File.Move(srcFileFullName, destFileFullName);
                    }
                    else
                    {
                        Log.WriteLine("Looks like file is already processed, the file will be there in source directory " + srcFileFullName, Directory.GetParent(srcFileFullName).Name, "", ServiceErrorType.ERROR);
                    }
                    break;
                case "delete":
                    if (ClsFileSystemWatcher.IsFileLocked(new FileInfo(srcFileFullName)))
                    {
                        System.Threading.Thread.Sleep(5000);
                    }
                    if (File.Exists(srcFileFullName))
                        File.Delete(srcFileFullName);
                    break;
		        default:
                    break;
            }	          
        }

        private static bool validateFileNameFormat(string filefullpath, string filename, out int clientId,  out int fileType, out string strTimeStamp,out string clientPrefix)
        {
            // Valid file format is JBRN_ProductImport_2016052_001.csv
            
            clientId = 0;
            fileType = 0;
            strTimeStamp = "";
            clientPrefix = "";
            /*****Read values from FileName with separation of "_" **********/

            string[] strarray = filename.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
            clientPrefix = strarray[0]; // Client prefix 
            string importTypeName = strarray[1]; // Import type name
            //strTimeStamp = strarray[2]; // time stamp

            
            //1. if filename client id and its parent folder name is same or not
            if( Directory.GetParent(filefullpath).Name.ToLower() != clientPrefix.ToLower() )
            {
                Log.WriteLine("Client Prefix of " + filename + " and directory associated does not match.." + filefullpath,clientPrefix,"",ServiceErrorType.ERROR);
                return false;
            }

            //2. Validate ClientId
            if (!ValidateClientPrefix(clientPrefix.ToLower(), out clientId)) return false;

            #region commented code
            //Check if importtype is available in DB
            //List<SqlParameter> parameters = new List<SqlParameter>();
            //parameters.Add(new SqlParameter("versionid", 2));
            //DataSet importTypes = utilities.RunSqlCommandDataSet(ImportDatabase + ".dbo.p_GetProcessImportTypes", CommandType.StoredProcedure, parameters);
            //DataRow[]  drcoll= importTypes.Tables[0].Select("ProcessImportTypeId= " + Convert.ToInt32( fileType).ToString());
            //if(drcoll.Count() ==0)
            //{
            //    //File Type not valid, log file format not valid error
            //    Log.WriteLine("Template Type not valid: " + filename);
            //    return false;
            //}
            #endregion

            //3. Validate Import Type
            try 
	        {
                ProcessImportTypes newProcesstype = (ProcessImportTypes)Enum.Parse(typeof(ProcessImportTypes), importTypeName);
                if (Enum.IsDefined(typeof(ProcessImportTypes), newProcesstype))
                {
                    fileType = (int) newProcesstype;
                }
               
	        }
	        catch (Exception )
	        {
		
		        fileType =0;
                Log.WriteLine("Template Type not valid: " + filename,clientPrefix,"",ServiceErrorType.ERROR);
                return false;
	        }
            

            return true;
        }

        private static bool ValidateClientPrefix (string clientPrefix , out int clientId )
        {
            clientId = Convert.ToInt32( GetClientIdfromPrefix(clientPrefix));
            if (clientId != 0)
                return true;
            else
                return false;
        }

        private static string GetClientIdfromPrefix(string clientPrefix)
        {
            string sql = "SELECT 	ClientID FROM 	" + OsCommonDatabase + ".dbo.Clients WHERE 	Prefix = '"+ clientPrefix +"' ";
            return utilities.RunSqlCommandScalarWithConnectionString(misc.getAppConfigValueString("OSCommonConnectionString"), sql, CommandType.Text);
        }

        public static Result RunImport(ImportFileAttrb objImportFileAttrb)
        {
            //Server.MapPath(uploadFileName), Convert.ToInt32(ddlImportType.SelectedValue), false, fupImportFile.FileName, scheduledDateTime
            Result result = ClsProcessImport.ImportExcel(objImportFileAttrb.FileName, objImportFileAttrb.ImportType, objImportFileAttrb.ValidateOnly, objImportFileAttrb.ShortFileName, objImportFileAttrb.ScheduledDate,objImportFileAttrb.FileExtension,objImportFileAttrb.ClientID);
            return result;
        }
    }

    public class ImportFileAttrb
    {
        public string FileName;
        public int ImportType;
        public bool ValidateOnly;
        public string ShortFileName;
        public DateTime? ScheduledDate;
        public string FileExtension;
        public int ClientID;
    }
}
